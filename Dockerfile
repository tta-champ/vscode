FROM ubuntu:jammy

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		ca-certificates \
		curl \
		netbase \
		wget \
# https://bugs.debian.org/929417
		tzdata \
	; \
	rm -rf /var/lib/apt/lists/*

RUN set -ex; \
	if ! command -v gpg > /dev/null; then \
		apt-get update; \
		apt-get install -y --no-install-recommends \
			gnupg \
			dirmngr \
		; \
		rm -rf /var/lib/apt/lists/*; \
	fi



RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		ca-certificates \
		iptables \
		openssl \
		pigz \
		xz-utils coreutils curl git wget htop gpg \
	  	apt-transport-https ca-certificates \
    		gnupg lsb-release sudo wget git zsh build-essential \
		lsb-release net-tools network-manager nmap npm \
		lsof sudo git nnn locales vim ssh tmux jq

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		golang python3 pip \
		sed tig nnn unzip \
		adr-tools tcpdump nginx 
		

# Set locale
RUN locale-gen en_US.UTF-8

RUN /bin/bash -c "curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash && pwd"
RUN chmod +x /root/.nvm/nvm.sh

RUN /bin/bash -c "$HOME/.nvm/nvm.sh install 18"

RUN groupadd --gid 1000 builder \
  && useradd --uid 1000 --gid builder --shell /bin/bash --create-home builder

# Set sudoer for "build"
RUN echo 'builder ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers


# docker
RUN mkdir -p /etc/apt/keyrings && \
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg  --dearmor -o /etc/apt/keyrings/docker.gpg && \
	echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
	apt-get update; apt-get -y upgrade && \
	apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose docker-compose-plugin

RUN set -eux; \
	usermod -aG docker builder  && newgrp docker &&\
	#curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb && \
	#dpkg -i minikube_latest_amd64.deb && \
	curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
	install minikube-linux-amd64 /usr/local/bin/minikube 
#	minikube start && \
#j	minikube kubectl -- get po -A && \
#	alias kubectl="minikube kubectl --" && \
#	minikube dashboard

USER builder

RUN sudo /root/.nvm/nvm.sh

RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" && \
	test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)" && \
	test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)" && \
	test -r ~/.bash_profile && echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >> ~/.bash_profile && \
	echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >> ~/.profile

WORKDIR /home/builder

ENTRYPOINT ["/bin/bash"]
